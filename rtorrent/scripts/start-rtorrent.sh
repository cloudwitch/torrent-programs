#!/usr/bin/env sh
if [ -f /home/rtorrent/.rt_session/rtorrent.lock ]; then
  rm /home/rtorrent/.rt_session/rtorrent.lock
fi

screen -dmS torrent rtorrent
mkdir -p /home/rtorrent/log/
touch /home/rtorrent/log/rtorrent.log
exec tail -f /home/rtorrent/log/rtorrent.log
